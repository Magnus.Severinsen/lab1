package rockPaperScissors;

import javax.swing.text.Style;
import java.util.ArrayList;
import java.util.Random;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    List<String> continueChoices = Arrays.asList("y","n");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true) {

            System.out.println(String.format("Let's play round %x", roundCounter));
            String human_choice = userChoice();
            String computer_choice = randomChoice();
            String choiseString = String.format("Human chose %s, computer chose %s. ", human_choice, computer_choice);
            // Check who won
            if (isWinning(human_choice, computer_choice)) {
                System.out.println(choiseString + "Human wins!");
                humanScore++;
            }
            else if (isWinning(computer_choice, human_choice)) {
                System.out.println(choiseString + "Computer wins!");
                computerScore++;
            }
            else {
                System.out.println(choiseString + " It's a tie!");
            }
            System.out.printf("Score: human %s, computer %s%n",computerScore,humanScore);
            roundCounter++;
            // Ask if they want to play again
            if (continuePlaying()){
                break;
            }
        }
        System.out.println("Bye bye :)");
    }

    public String randomChoice() {
        // Do something to add random...
        Random random = new Random();
        int randomInt = random.nextInt(0,rpsChoices.size());
        return rpsChoices.get(randomInt);

    }

    public String userChoice() {
        return readInput("Your choice (Rock/Paper/Scissors)?");
    }

    public boolean isWinning (String choice1, String choice2) {
        if (choice1.equals("paper")) {
            return choice2.equals("rock");
        }else if (choice1.equals("scissors")) {
            return choice2.equals("paper");
        }else {
            return choice2.equals("scissors");
        }
    }

    public boolean continuePlaying(){
        String continueAns = readInput("Do you wish to continue playing? (y/n)?");
        return continueAns.equals("n");
        /**
        while (true) {
            String continueAns = readInput("Continue (y/n)?");
            /if (validate_input(continueAns)) {
                return continueAns;
            }

        }**/

    }


    public boolean validate_input(String input, String[] inputList) {
        return input.equals("y") || input.equals("n");
    }


    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
